// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for macos - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyBaCRFZCj_qK70YkBTRzJGuLf3AhW1wfZs',
    appId: '1:1053622597578:web:48fdeb720937bd0efe4133',
    messagingSenderId: '1053622597578',
    projectId: 'liquid-muse-345411',
    authDomain: 'liquid-muse-345411.firebaseapp.com',
    storageBucket: 'liquid-muse-345411.appspot.com',
    measurementId: 'G-J78BDSJRBS',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyCqcFO__5uaNf6QodQ-hb7s5CcbEKa-VE4',
    appId: '1:1053622597578:android:3d1996bdd8cdb0cefe4133',
    messagingSenderId: '1053622597578',
    projectId: 'liquid-muse-345411',
    storageBucket: 'liquid-muse-345411.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyDVEEaU4iaRus8In9C9zE4dFd_QLiyA2tM',
    appId: '1:1053622597578:ios:9c8a6914afc8c452fe4133',
    messagingSenderId: '1053622597578',
    projectId: 'liquid-muse-345411',
    storageBucket: 'liquid-muse-345411.appspot.com',
    iosClientId: '1053622597578-uthanh9d5v6h1451sdcfvhsrrec80qfj.apps.googleusercontent.com',
    iosBundleId: 'com.example.flashChat',
  );
}
