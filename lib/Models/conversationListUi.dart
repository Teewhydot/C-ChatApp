import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flash_chat/Models/constants.dart';
import 'package:flash_chat/UI/chat-screen.dart';
import 'package:flutter/material.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:progress_indicator_button/progress_button.dart';
import 'package:toast/toast.dart';

class ConversationList extends StatefulWidget {
  final String name;

  const ConversationList(this.name, {Key? key}) : super(key: key);
  @override
  ConversationListState createState() => ConversationListState();
}

class ConversationListState extends State<ConversationList> {
  final fireStore = FirebaseFirestore.instance;

  Future<void> deleteClone(String cloneName) {
    return fireStore.collection('clones')
        .doc(cloneName)
        .delete().then((value) => Toast.show(
        'Clone deleted',
        duration: 2, gravity: Toast.center, backgroundColor: Colors.red));
  }
  @override
  Widget build(BuildContext context) {

    Widget buildAlertDialog(BuildContext context) {
      bool hasInternet;
      return AlertDialog(
        title: const Text('Warning'),
        content: const Text(
            'Are you sure you want to delete this clone? This action is irreversible'),
        actions: [
         SizedBox(
           width: 75,
           height: 25,
           child: ProgressButton(
             color: Colors.transparent,
              borderRadius: const BorderRadius.all(Radius.circular(10)),
              onPressed: (AnimationController controller ) {
               Navigator.pop(context);
              },
              child: const Text(
                'No',
                style: TextStyle(color: Colors.blue),
              ),
           )
         ),
          SizedBox(
            width: 75,
            height: 25,
            child: ProgressButton(
              color: Colors.transparent,
              borderRadius: const BorderRadius.all(Radius.circular(10)),
              strokeWidth: 2,
              child: const Text(
                "Yes",
                style: TextStyle(
                  color: Colors.blue,
                ),
              ),
              onPressed: (AnimationController controller) async {
                if (controller.isCompleted) {
                  controller.reverse();
                } else {
                  controller.forward();
                  hasInternet =
                  await InternetConnectionChecker().hasConnection;
                  if (hasInternet == false) {
                    Toast.show("No internet connection",
                        duration: Toast.lengthShort, gravity: Toast.center, backgroundColor: Colors.red);
                  } else {
                    await deleteClone(widget.name);
                    controller.reset();
                    Navigator.pop(context);
                  }
                }
              },
            ),
          ),
        ],
      );
    }

    return GestureDetector(
      onLongPress: () {
        showDialog(context: context, builder: buildAlertDialog);
      },
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => ChatScreen(widget.name)));
      },
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              Expanded(
                child: Row(
                  children: <Widget>[
                    const CircleAvatar(
                      maxRadius: 30,
                    ),
                    addHorizontalSpacing(15),
                    Expanded(
                      child: Container(
                        color: Colors.transparent,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              widget.name,
                              style: const TextStyle(fontSize: 16),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
